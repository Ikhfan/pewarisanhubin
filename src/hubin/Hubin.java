package hubin;
import javax.swing.JOptionPane;
class HubinSekolah {
    private String promosi,prakerin="Bulan Juli sampai Bulan Oktober",rekruitmen,beasiswa;    
    //setter
    public void setPromosi(String promosi) {
        this.promosi = promosi;
    }
    public void setPrakerin(String prakerin) {
        this.prakerin = prakerin;
    }
    public void setRekruitmen(String rekruitmen) {
        this.rekruitmen = rekruitmen;
    }
    public void setBeasiswa(String beasiswa) {
        this.beasiswa = beasiswa;
    }
    //getter
    public String getPromosi() {
        return promosi;
    }
    public String getPrakerin() {
        return prakerin;
    }
    public String getRekruitmen() {
        return rekruitmen;
    }
    public String getBeasiswa() {
        return beasiswa;
    }
    public void tampilkanprakerin() {
    System.out.println("Pelaksanaan Prakerin = " + getPrakerin());
    }
}
// overloading
class HubunganIndustri{
    public void promosi (){
        String Promosi = JOptionPane.showInputDialog("Input fungsi promosi");
        System.out.println("Fungsi Promosi: "+ Promosi);
    }
    public void prakerin (){
        String Prakerin = JOptionPane.showInputDialog("Input fungsi prakerin");
        System.out.println("Fungsi Prakerin: "+ Prakerin);
    }
    public void rekruitmen (){
        String Rekruitmen = JOptionPane.showInputDialog("Input fungsi rekruitmen");
        System.out.println("Fungsi Rekruitmen: "+ Rekruitmen);
    }
    public void beasiswa (){
        String Beasiswa = JOptionPane.showInputDialog("Input fungsi beasiswa");
        System.out.println("Fungsi Beasiswa: "+ Beasiswa);
    }
}
class prakerin extends HubinSekolah {
    private String prakerin;
    public void tampilkanPrakerin(){
        super.tampilkanprakerin();
        System.out.println("Pelaksanaan Prakerin =  " + getPrakerin());
    }
}
public class Hubin {
    public static void main(String[] args) {
        // TODO code application logic here
        HubunganIndustri hi = new HubunganIndustri();
        hi.promosi();
        hi.prakerin();
        hi.rekruitmen();
        hi.beasiswa();
        prakerin p = new prakerin();
        p.tampilkanPrakerin();
    }
    
}
